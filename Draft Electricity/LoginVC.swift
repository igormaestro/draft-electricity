//
//  LoginVC.swift
//  Draft Electricity
//
//  Created by Igor Litvin on 4/7/19.
//  Copyright © 2019 Igor Litvin. All rights reserved.
//

import UIKit
import Parse

class LoginVC: UIViewController {
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTF.text = ""
        passwordTF.text = ""
    }
    
    @IBAction func signIn(_ sender: Any) {
        let sv = UIViewController.displaySpinner(onView: self.view)
        PFUser.logInWithUsername(inBackground: userNameTF.text!, password: passwordTF.text!) { (user, error) in
            UIViewController.removeSpinner(spinner: sv)
            if user != nil {
                NotificationCenter.default.post(name: Notification.Name.loginSucceed, object: nil)
                self.dismiss(animated: true, completion: nil)
            }else{
                if let descrip = error?.localizedDescription{
                    self.displayErrorMessage(message: (descrip))
                }
            }
        }
    }
    
    @IBAction func signUp(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
//        let user = PFUser()
//        user.username = userNameTF.text
//        user.password = passwordTF.text
//        let sv = UIViewController.displaySpinner(onView: self.view)
//        user.signUpInBackground { (success, error) in
//            UIViewController.removeSpinner(spinner: sv)
//            if success{
//                NotificationCenter.default.post(name: Notification.Name.loginSucceed, object: nil)
//                self.dismiss(animated: true, completion: nil)
//            }else{
//                if let descrip = error?.localizedDescription{
//                    self.displayErrorMessage(message: descrip)
//                }
//            }
//        }
    }
    
    func displayErrorMessage(message:String) {
        let alertView = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertView.addAction(OKAction)
        if let presenter = alertView.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        self.present(alertView, animated: true, completion:nil)
    }
    
}

extension UIViewController {
    
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}

extension Notification.Name {
    static let loginSucceed = Notification.Name("loginSucceed")
    static let dataRecived = Notification.Name("dataRecived")
}

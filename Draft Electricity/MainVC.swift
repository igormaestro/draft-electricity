//
//  MainVC.swift
//  Draft Electricity
//
//  Created by Igor Litvin on 10/28/18.
//  Copyright © 2018 Igor Litvin. All rights reserved.
//

import UIKit
import GoogleMaps

class MainVC: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var GMapView: GMSMapView!
    
    @IBOutlet weak var myTB: UIToolbar!
    
    var cameraStartPosition = GlobalObjects.Camera.init(latitude: 50.366027, longtitude: 30.4526258)
    var locationManager = CLLocationManager()
    var needLocation:Bool = true
    
    var needToLoad: Bool = false
    
    enum buttonState{
        case defaultState  // key not pressed
        case oneTimePressed  // pressed first time
        case secondTimePressed // pressed second time
    }
    
    var buttonSquareSate = buttonState.defaultState
    var buttonTriangleSate = buttonState.defaultState
    var buttonCircleState = buttonState.defaultState
    var buttonRhombState = buttonState.defaultState
    var buttonTextState = buttonState.defaultState
    var buttonRotateSate = buttonState.defaultState
    
    var lableText = "empty"
    
    var triangles:Array<Data> = []
    var squares:Array<Data> = []
    var circles:Array<Data> = []
    var rhombs:Array<Data> = []
    var texts:Array<Data> = []
    var cameras:Array<Data> = []
    var lines:Array<Data> = []
    
    var path = GMSMutablePath()
    
    var transform:CGAffineTransform = CGAffineTransform.init(a: 1.0, b: 0.0, c: 0.0, d: 1.0, tx: 0.0, ty: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        UserDefaults.standard.removeObject(forKey: GlobalStrings().keysForSave)
        //        let _ = UIRotationGestureRecognizer.init(target: self, action: #selector(rotation(_:)))
        UIApplication.shared.isIdleTimerDisabled = true
        let _ = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(showTextField))
        // Do any additional setup after loading the view, typically from a nib.
        let camera = GMSCameraPosition.camera(withLatitude: self.cameraStartPosition.latitude, longitude: self.cameraStartPosition.longtitude, zoom: 19.0)
        GMapView.camera = camera
        GMapView.settings.rotateGestures = false
        GMapView.settings.tiltGestures = false
        
        if needToLoad{
            needToLoad = false
            loadCoordinates()
            loadMarkers()
        }
        //        GMapView.animate(to: camera)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        self.myTB?.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        if needLocation{
            self.GMapView?.isMyLocationEnabled = true
            self.locationManager.delegate = self
            self.locationManager.startUpdatingLocation()
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let location = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: location?.coordinate.latitude ?? cameraStartPosition.latitude, longitude: location?.coordinate.longitude ?? cameraStartPosition.longtitude, zoom: 19.0)
        GMapView.animate(to: camera)
        self.locationManager.stopUpdatingLocation()
    }
    
    func buildingCoordinateArray(){
        
        let cameraPosition = GMapView.camera.target
        let camera = GlobalObjects.Camera.init(latitude: cameraPosition.latitude, longtitude: cameraPosition.longitude)
        let cameraData = try! PropertyListEncoder().encode(camera)
        cameras.append(cameraData)
    }
    
    func loadCoordinates(){
        let camera = GMSCameraPosition.camera(withLatitude: cameraStartPosition.latitude, longitude:cameraStartPosition.longtitude , zoom: 19.0)
        GMapView.camera = camera
    }
    
    func loadShapes<T: Shape>(_ type: T.Type, from shapes: inout [Data])  {
        shapes.forEach { shape in
            if let decodedShape = try? PropertyListDecoder().decode(T.self, from: shape) {
                putShape(shape: decodedShape, nedAppend: false, appendArray: &shapes)
            }
        }
    }
    
    func loadTexts<T: Shape>(_ tepe: T.Type, from text: inout [Data]){
        text.forEach {text in
            if let decodedText = try? PropertyListDecoder().decode(GlobalObjects.Text.self, from: text) {
                putText(latitude: decodedText.latitude, longitude: decodedText.longtitude, transform: decodedText.transform, text: decodedText.text, nedAppend: false)
            }
        }
    }
    
    func loadMarkers(){
        // load Shapes
        loadShapes(GlobalObjects.Square.self, from:  &squares)
        loadShapes(GlobalObjects.Triangle.self, from: &triangles)
        loadShapes(GlobalObjects.Circle.self, from: &circles)
        loadShapes(GlobalObjects.Rhomb.self, from: &rhombs)
        loadTexts(GlobalObjects.Text.self, from: &texts)
        
        // load lines
        for count in 0..<lines.count{
            path.removeAllCoordinates()
            print(" in line array -  \(lines.count) items")
            let data = lines[count]
            let decodedLine = try! PropertyListDecoder().decode(GlobalObjects.Line.self, from: data)
            let loadPath = GMSMutablePath()
            loadPath.add(CLLocationCoordinate2D.init(latitude: decodedLine.firstCoordinateLatitude, longitude: decodedLine.firstCoordinateLongtitude))
            loadPath.add(CLLocationCoordinate2D.init(latitude: decodedLine.seconCoordinateLatitude, longitude: decodedLine.seconCoordinateLongtitude))
            let line = GMSPolyline.init(path: loadPath)
            line.map = GMapView
            path.add(CLLocationCoordinate2D.init(latitude: decodedLine.seconCoordinateLatitude, longitude: decodedLine.seconCoordinateLongtitude))
        }
        
        path.removeAllCoordinates()
        
    }
    
    func scrennShot(){
        //        let staticMapUrl: String = "http://maps.google.com/maps/api/staticmap?markers=\(cameraPositionLatitude),\(cameraPositionLongtitude)&\("zoom=15&size=\(2 * Int(GMapView.frame.size.width))x\(2 * Int(GMapView.frame.size.height))")&sensor=true"
        //        let mapUrl: NSURL = NSURL(string: staticMapUrl)!
        //        print(mapUrl)
    }
    
    func autosave(){
        buildingCoordinateArray()
        let autoSave = AutosaveProject.init(rhombs: rhombs, circles: circles, squares: squares, triangles: triangles, texts: texts, cameras: cameras, lines: lines)
        autoSave.autosave()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("___________")
        if segue.identifier == "goToMenu"{
            if let viewController = segue.destination as? MenuVC{
                buildingCoordinateArray()
                
                viewController.squares = squares
                viewController.circles = circles
                viewController.rhombs = rhombs
                viewController.triangles = triangles
                viewController.texts = texts
                viewController.cameras = cameras
                viewController.lines = lines
            }
        }
    }
    
    @IBAction func square(_ sender: Any) {
        
        switch buttonSquareSate {
        case .defaultState:
            let viewCenter = GMapView.center
            GMapView.animate(toZoom: 19)
            let react = CGRect.init(x: viewCenter.x - 5, y: viewCenter.y - 20, width: 10, height: 20)
            let box = UIView.init(frame: react)
            box.backgroundColor = UIColor.black
            box.alpha = 0.1
            box.tag = 100
            box.isUserInteractionEnabled = true
            box.transform = transform
            GMapView.addSubview(box)
            buttonSquareSate = .oneTimePressed
            print(box.tag)
            return
        case .oneTimePressed:
            let coordinate = GMapView.projection.coordinate(for: GMapView.center)
            putSquare(latitude: coordinate.latitude, longitude: coordinate.longitude, transform: transform, nedAppend: true)
            autosave()
            return
        case .secondTimePressed:
            return
        }
    }
    
    @IBAction func triangle(_ sender: Any) {
        switch buttonTriangleSate {
        case .defaultState:
            GMapView.animate(toZoom: 19)
            let center = GMapView.center
            let triangle = TriangleView(frame: CGRect(x: center.x - 10, y: center.y - 20, width: 20 , height: 20))
            triangle.backgroundColor = .white
            triangle.alpha = 0.3
            triangle.tag = 100
            triangle.transform = transform
            GMapView.addSubview(triangle)
            buttonTriangleSate = .oneTimePressed
            return
        case .oneTimePressed:
            let coordinate = GMapView.projection.coordinate(for: GMapView.center)
            putTriangle(latitude: coordinate.latitude, longitude: coordinate.longitude, transform: transform, nedAppend: true)
            autosave()
            return
        case .secondTimePressed:
            return
        }
    }
    
    @IBAction func FixElement(_ sender: Any) {
        GMapView.animate(toZoom: 19)
        buttonCircleState = .defaultState
        buttonTriangleSate = .defaultState
        buttonSquareSate = .defaultState
        buttonRotateSate = .defaultState
        buttonRhombState = .defaultState
        buttonTextState = .defaultState
        
        if let viewWithTag = self.view.viewWithTag(100){
            viewWithTag.removeFromSuperview()
        }
        
        if let viewWithTag = self.view.viewWithTag(101){
            viewWithTag.removeFromSuperview()
        }
    }
    
    @IBAction func circle(_ sender: Any) {
        switch buttonCircleState {
        case .defaultState:
            GMapView.animate(toZoom: 19)
            let center = GMapView.center
            let react = CGRect.init(x: center.x - 1, y: center.y + 53, width: 20, height: 20)
            let circleLayer = CircleView.init(frame: react)
            circleLayer.circle(cX: 1, cY: 1, radius: 9, color: UIColor.blue.cgColor)
            circleLayer.alpha = 0.3
            circleLayer.tag = 100
            view.addSubview(circleLayer)
            buttonCircleState = .oneTimePressed
            return
        case .oneTimePressed:
            let coordinate = GMapView.projection.coordinate(for: GMapView.center)
            putCircle(latitude: coordinate.latitude, longitude: coordinate.longitude, nedAppend: true)
            autosave()
            return
        case .secondTimePressed:
            return
        }
    }
    
    @IBAction func line(_ sender: Any) {
        path.removeAllCoordinates()
        showLineClearMassege()
    }
    
    @IBAction func rhomb(_ sender: Any) {
        switch buttonRhombState {
        case .defaultState:
            GMapView.animate(toZoom: 19)
            let center = GMapView.center
            let rhomb = RhombView(frame: CGRect(x: center.x - 5, y: center.y - 20, width: 10 , height: 20))
            rhomb.backgroundColor = UIColor.init(white: 1, alpha: 0)
            rhomb.alpha = 0.3
            rhomb.tag = 100
            rhomb.transform = transform
            GMapView.addSubview(rhomb)
            buttonRhombState = .oneTimePressed
            return
        case .oneTimePressed:
            let coordinate = GMapView.projection.coordinate(for: GMapView.center)
            putRhomb(latitude: coordinate.latitude, longitude: coordinate.longitude, transform: transform, nedAppend: true)
            autosave()
            return
        case .secondTimePressed:
            return
        }
    }
    
    @IBAction func distance(_ sender: Any) {
        switch buttonTextState {
        case .defaultState:
            GMapView.animate(toZoom: 19)
            showTextField()
            buttonTextState = .oneTimePressed
            return
        case .oneTimePressed:
            let center = GMapView.center
            let react = CGRect.init(x: center.x - 50, y: center.y - 40, width: 100, height: 40)
            let comment = UILabel.init(frame: react)
            let float = CGFloat.init(integerLiteral: 10)
            comment.text = "\(lableText)"
            comment.font = UIFont.boldSystemFont(ofSize: float)
            comment.tag = 100
            comment.transform = transform
            GMapView.addSubview(comment)
            buttonTextState = .secondTimePressed
            return
        case .secondTimePressed:
            let coordinate = GMapView.projection.coordinate(for: GMapView.center)
            putText(latitude: coordinate.latitude, longitude: coordinate.longitude, transform: transform, text: lableText, nedAppend: true)
            buttonTextState = .defaultState
            autosave()
            return
        }
        
    }
    
    @IBAction func rotationViewAdd(_ sender: Any) {
        switch buttonRotateSate {
        case .defaultState:
            let viewCenter = GMapView.center
            let grayBox = UIView.init(frame: CGRect.init(x: viewCenter.x - 100, y: viewCenter.y - 40, width: 200, height: 200))
            grayBox.backgroundColor = UIColor.gray
            grayBox.alpha = 0.3
            grayBox.tag = 101
            let recognizer = UIRotationGestureRecognizer.init(target: self, action: #selector(rotation(_:)))
            grayBox.addGestureRecognizer(recognizer)
            view.addSubview(grayBox)
            buttonRotateSate = .oneTimePressed
            GMapView.setMinZoom(19.0, maxZoom: 19.0)
            GMapView.animate(toZoom: 19)
            return
        case .oneTimePressed:
            if let grayBox = view.viewWithTag(101){
                grayBox.removeFromSuperview()
                buttonRotateSate = .defaultState
                GMapView.setMinZoom(14.0, maxZoom: 19.0)
            }
            return
        case .secondTimePressed:
            return
        }
    }
    
    @objc func showTextField(){
        let allert = UIAlertController(title: "111", message: "222", preferredStyle: .alert)
        allert.addTextField { (textField1) in
            textField1.placeholder = "Distance"
        }
        allert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        allert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (act) in
            if let text = allert.textFields?.first?.text{
                self.lableText = text
                self.distance((Any).self)
            }
        }))
        present(allert, animated: true, completion: nil)
        
    }
    
    @objc func showLineClearMassege(){
        let alert = UIAlertController.init(title: "", message: "Line is clear", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @objc func rotation(_ sender: UIRotationGestureRecognizer){
        if let elementView = view.viewWithTag(100){
            elementView.transform = elementView.transform.rotated(by: sender.velocity / 30)
            transform = elementView.transform
        }
    }
    
    func putShape<T: Shape>(shape: T, nedAppend:Bool, appendArray: inout [Data] ){
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: shape.latitude, longitude: shape.longtitude)
        let markerBox = shape.markerView
        markerBox.transform = shape.transform
        marker.iconView = markerBox
        marker.map = GMapView
    }
    
    func putSquare(latitude:CLLocationDegrees, longitude:CLLocationDegrees, transform:CGAffineTransform, nedAppend:Bool){
        let marker = GMSMarker()
        let coordinateForPath = CLLocationCoordinate2D(latitude: latitude + 0.00003200000000 , longitude: longitude + 0.00000000002207)
        path.add(coordinateForPath)
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let react = CGRect.init(x: 200, y: 200, width: 10, height: 20)
        let markerBox = UIView.init(frame: react)
        markerBox.backgroundColor = UIColor.black
        markerBox.transform = transform
        marker.iconView = markerBox
        marker.map = GMapView
        buttonSquareSate = .defaultState
        if nedAppend{
            if let viewWithTag = self.view.viewWithTag(100){
                viewWithTag.removeFromSuperview()
            }
            let square = GlobalObjects.Square.init(latitude: latitude, longtitude: longitude, transform: transform)
            let dataSquare = try! PropertyListEncoder().encode(square)
            squares.append(dataSquare)
            putLine()
        }
    }
    
    func putTriangle(latitude:CLLocationDegrees, longitude:CLLocationDegrees, transform:CGAffineTransform, nedAppend:Bool){
        let marker = GMSMarker()
        let coordinateForPath = CLLocationCoordinate2D(latitude: latitude  + 0.00001600000000 /* + 0.00003200000000 */, longitude: longitude /* + 0.00000000002207 */)
        path.add(coordinateForPath)
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let markerTriangle = TriangleView(frame: CGRect(x: 200, y: 200, width: 20 , height: 20))
        markerTriangle.backgroundColor = UIColor(white: 1, alpha: 0)
        markerTriangle.transform = transform
        marker.iconView = markerTriangle
        marker.map = GMapView
        buttonTriangleSate = .defaultState
        if nedAppend{
            if let viewWithTag = self.view.viewWithTag(100){
                viewWithTag.removeFromSuperview()
            }
            let triangle = GlobalObjects.Triangle.init(latitude: latitude, longtitude: longitude, transform: transform)
            let dataTriangle = try! PropertyListEncoder().encode(triangle)
            triangles.append(dataTriangle)
            putLine()
        }
    }
    
    func putCircle(latitude:CLLocationDegrees, longitude:CLLocationDegrees, nedAppend:Bool){
        let marker = GMSMarker()
        let coordinateForPath = CLLocationCoordinate2D(latitude: latitude + 0.00003200000000 , longitude: longitude + 0.00000000002207)
        path.add(coordinateForPath)
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let circleLayer = CircleView.init(frame: CGRect.init(x: 1, y: 1, width: 20, height: 20))
        circleLayer.circle(cX: 10, cY: 10, radius: 9, color: UIColor.orange.cgColor)
        marker.iconView = circleLayer
        marker.map = GMapView
        print(marker)
        buttonCircleState = .defaultState
        if nedAppend{
            if let viewWithTag = self.view.viewWithTag(100){
                viewWithTag.removeFromSuperview()
            }
            let circle = GlobalObjects.Circle.init(latitude: latitude, longtitude: longitude, transform: transform)
            let dataCircle = try! PropertyListEncoder().encode(circle)
            circles.append(dataCircle)
            print(transform)
            putLine()
        }
    }
    
    func putRhomb(latitude:CLLocationDegrees, longitude:CLLocationDegrees, transform:CGAffineTransform, nedAppend:Bool){
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let react = CGRect.init(x: 1, y: 1, width: 10, height: 20)
        let rhombLayer = RhombView.init(frame: react)
        rhombLayer.backgroundColor = UIColor.init(white: 1, alpha: 0)
        rhombLayer.transform = transform
        print(rhombLayer.transform)
        marker.iconView = rhombLayer
        marker.map = GMapView
        buttonRhombState = .defaultState
        if nedAppend{
            if let viewWithTag = self.view.viewWithTag(100){
                viewWithTag.removeFromSuperview()
            }
            let rhomb = GlobalObjects.Rhomb.init(latitude: latitude, longtitude: longitude, transform: transform)
            let dataRhomb = try! PropertyListEncoder().encode(rhomb)
            rhombs.append(dataRhomb)
            
        }
    }
    
    func putText(latitude:CLLocationDegrees, longitude:CLLocationDegrees, transform:CGAffineTransform, text:String, nedAppend:Bool){
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let react = CGRect.init(x: 10, y: 10, width: 100, height: 40)
        let comment = UILabel.init(frame: react)
        let float = CGFloat.init(integerLiteral: 10)
        self.lableText = text
        comment.text = "\(lableText)"
        comment.font = UIFont.boldSystemFont(ofSize: float)
        comment.transform = transform
        marker.iconView = comment
        marker.map = GMapView
        buttonTextState = .defaultState
        if nedAppend{
            if let viewWithTag = self.view.viewWithTag(100){
                viewWithTag.removeFromSuperview()
                let text = GlobalObjects.Text.init(latitude: latitude, longtitude: longitude, transform: transform, text: lableText)
                let dataText = try! PropertyListEncoder().encode(text)
                texts.append(dataText)
            }
        }
    }
    
    func putLine(){
        if path.count() == 2{
            let line = GMSPolyline.init(path: path)
            line.map = GMapView
            let lineForSave = GlobalObjects.Line.init(firstCoordinateLatitude: path.coordinate(at: 0).latitude, firstCoordinateLongtitude: path.coordinate(at: 0).longitude, seconCoordinateLatitude: path.coordinate(at: 1).latitude, seconCoordinateLongtitude: path.coordinate(at: 1).longitude)
            let lineData = try! PropertyListEncoder().encode(lineForSave)
            lines.append(lineData)
            path.removeCoordinate(at: 0)
        }
    }
    
}

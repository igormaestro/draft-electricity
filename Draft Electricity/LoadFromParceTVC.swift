//
//  LoadFromParceTVC.swift
//  Draft Electricity
//
//  Created by Igor Litvin on 4/7/19.
//  Copyright © 2019 Igor Litvin. All rights reserved.
//

import UIKit
import Parse

class LoadFromParceTVC: UITableViewController {
    
    var tableArray:[PFObject] = []
    var streetName:[String] = []
    var project = GlobalObjects.Project()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return streetName.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let words = streetName[indexPath.row]
        cell.textLabel?.text = words
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let parseObject = tableArray[indexPath.row]
        let projectParseData = parseObject["data"] as! PFFileObject
        let projectData = try! projectParseData.getData()
        project = try! PropertyListDecoder().decode(GlobalObjects.Project.self, from: projectData)
        self.performSegue(withIdentifier: "loadCoordinatesFromParce", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let selectedWords = streetName[indexPath.row]
            for count in 0..<streetName.count{
                print(count)
                if streetName[count] == selectedWords{
                    print(streetName[count])
                    print("**************************")
                    print("\(streetName[count]) at count \(count)")
                    streetName.remove(at: count)
                    let query = PFQuery(className: "Project")
                    query.whereKey("streetName", equalTo: selectedWords)
                    query.findObjectsInBackground {
                        (objects, error) -> Void in
                        for object in objects! {
                            object.deleteEventually()
                            print("deleted \(selectedWords)")
                        }
                    }
                    tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
                    return
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("-------------> MainVC ------------->")
        if segue.identifier == "loadCoordinatesFromParce"{
            if let viewController = segue.destination as? MainVC{
                viewController.rhombs = project.rhombs
                viewController.circles = project.circles
                viewController.squares = project.squares
                viewController.triangles = project.triangles
                viewController.texts = project.texts
                viewController.lines = project.lines
                viewController.needToLoad = true
                viewController.needLocation = false
                viewController.cameraStartPosition = try! PropertyListDecoder().decode(GlobalObjects.Camera.self, from: project.cameras[0])
            }
        }
    }
    
}

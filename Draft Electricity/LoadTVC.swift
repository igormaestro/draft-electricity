//
//  LoadTVC.swift
//  Draft Electricity
//
//  Created by Igor Litvin on 12/22/18.
//  Copyright © 2018 Igor Litvin. All rights reserved.
//

import UIKit

class LoadTVC: UITableViewController {
    
    var keysArray:[String] = []
    var project = GlobalObjects.Project()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadKeysArray()
    }
    
    func loadKeysArray(){
        let loadKeys = LoadSavedKeys().loadKeysArray()
        keysArray = loadKeys
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("-------------> MainVC ------------->")
        if segue.identifier == "loadCoordinates"{
            if let viewController = segue.destination as? MainVC{
                viewController.rhombs = project.rhombs
                viewController.circles = project.circles
                viewController.squares = project.squares
                viewController.triangles = project.triangles
                viewController.texts = project.texts
                viewController.lines = project.lines
                viewController.needToLoad = true
                viewController.needLocation = false
                viewController.cameraStartPosition = try! PropertyListDecoder().decode(GlobalObjects.Camera.self, from: project.cameras[0])
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let selectedWords = keysArray[indexPath.row]
            for count in 0..<keysArray.count{
                if keysArray[count] == selectedWords{
                    keysArray.remove(at: count)
                    UserDefaults.standard.set(self.keysArray, forKey: GlobalStrings().keysForSave)
                    UserDefaults.standard.removeObject(forKey: selectedWords)
                    tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
                    return
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return keysArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let words = keysArray[indexPath.row]
        cell.textLabel?.text = words
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedWords = keysArray[indexPath.row]
        let projectData = UserDefaults.standard.data(forKey: selectedWords)        
        project = try! PropertyListDecoder().decode(GlobalObjects.Project.self, from: projectData!)
        print(project)
        self.performSegue(withIdentifier: "loadCoordinates", sender: self)
    }

}

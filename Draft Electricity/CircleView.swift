//
//  CircleView.swift
//  homeWorks
//
//  Created by Igor Litvin on 11/1/18.
//  Copyright © 2018 Igor Litvin. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class CircleView: UIView {
    
    func circle(cX: Int, cY: Int, radius: Int, color: CGColor){
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: cX,y: cY), radius: CGFloat(radius), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = color
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1.0
        layer.addSublayer(shapeLayer)
    }

}

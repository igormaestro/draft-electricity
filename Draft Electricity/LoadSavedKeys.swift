//
//  LoadSavedKeys.swift
//  Draft Electricity
//
//  Created by Igor Litvin on 12/31/18.
//  Copyright © 2018 Igor Litvin. All rights reserved.
//

import UIKit

class LoadSavedKeys: NSObject {
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func loadKeysArray() -> Array<String>{
        var loadKeysArray: Array<String> = []
        if isKeyPresentInUserDefaults(key: GlobalStrings().keysForSave){
            let loadKeys = (UserDefaults.standard.array(forKey: GlobalStrings().keysForSave)! as Array) as! [String]
            loadKeysArray = loadKeys
        }
        else{
            print("No save data found")
        }
        return loadKeysArray
    }
}

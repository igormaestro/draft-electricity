//
//  AutosaveProject.swift
//  Draft Electricity
//
//  Created by Igor Litvin on 1/9/19.
//  Copyright © 2019 Igor Litvin. All rights reserved.
//

import UIKit
import CoreLocation

var initTriangles:Array<Data> = []
var initSquares:Array<Data> = []
var initCircles:Array<Data> = []
var initRhombs:Array<Data> = []
var initTexts:Array<Data> = []
var initCameras:Array<Data> = []
var initLines:Array<Data> = []
var initMasterLineCoordinates:[String: Array<Array<CLLocationDegrees>>] = [:]

class AutosaveProject: NSObject {
    
    func autosave(){
        let autosaveProject = GlobalObjects.Project.init(rhombs: initRhombs, circles: initCircles, squares: initSquares, triangles: initTriangles, texts: initTexts, cameras: initCameras, lines: initLines)
        let dataAutosaveProject = try! PropertyListEncoder().encode(autosaveProject)
        UserDefaults.standard.set(dataAutosaveProject, forKey: GlobalStrings().keyForAutosave)
        if !isKeyPresentInUserDefaults(key: GlobalStrings().keysForSave){
            let newArrLoadKeys:Array<String> = []
            UserDefaults.standard.set(newArrLoadKeys, forKey: GlobalStrings().keysForSave)
        }
        var loadKeys = (UserDefaults.standard.array(forKey: GlobalStrings().keysForSave)! as Array) as! [String]
        if loadKeys.contains(GlobalStrings().keyForAutosave){
            return
        }
        else{
            loadKeys.append(GlobalStrings().keyForAutosave)
        }
        UserDefaults.standard.set(loadKeys, forKey: GlobalStrings().keysForSave)
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    init(rhombs: Array<Data>, circles: Array<Data>, squares: Array<Data>, triangles: Array<Data>, texts: Array<Data>, cameras: Array<Data>, lines:Array<Data> ) {
        initTriangles = triangles
        initSquares = squares
        initCircles = circles
        initRhombs = rhombs
        initTexts = texts
        initCameras = cameras
        initLines = lines
    }
}

//
//  TriangleView.swift
//  homeWorks
//
//  Created by Igor Litvin on 10/31/18.
//  Copyright © 2018 Igor Litvin. All rights reserved.
//

import UIKit

class RhombView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY / 2))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY / 2))
        context.addLine(to: CGPoint(x: (rect.maxX / 2.0), y: rect.minY))
        context.move(to: CGPoint(x: rect.maxX, y: rect.maxY / 2))
        context.addLine(to: CGPoint(x: rect.minX, y: rect.maxY / 2))
        context.addLine(to: CGPoint(x: (rect.maxX / 2), y: rect.maxY))
        context.closePath()
        context.setFillColor(red: 0.0, green: 0.0, blue: 1.0, alpha: 1)
        context.fillPath()
    }
    
}




//
//  ViewController.swift
//  Draft Electricity
//
//  Created by Igor Litvin on 10/25/18.
//  Copyright © 2018 Igor Litvin. All rights reserved.
//

import UIKit
import GoogleMaps
import Parse

class MenuVC: UIViewController {
    @IBOutlet weak var loadFromParceButton: UIButton!
    @IBOutlet weak var loginTF: UIButton!
    
    let keys = GlobalStrings()
    var keysArray:Array<String> = []
    
    var masterLineCoordinates = [String: Array<Array<CLLocationDegrees>>]()
    
    var rhombs:Array<Data> = []
    var circles:Array<Data> = []
    var squares:Array<Data> = []
    var triangles:Array<Data> = []
    var texts:Array<Data> = []
    var cameras:Array<Data> = []
    var lines:Array<Data> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(loginSucceed(notification:)), name: Notification.Name.loginSucceed, object: nil)
        loadKeysArray()
        loginButtonText()
        showLoadFromParceButton()
        
    }
    
    func loadKeysArray(){
        let loadKeys = LoadSavedKeys().loadKeysArray()
        keysArray = loadKeys
    }
    
    @objc func showTextField(){
        let allert = UIAlertController(title: "Save project", message: "", preferredStyle: .alert)
        allert.addTextField { (textField1) in
            textField1.placeholder = "project name"
        }
        allert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        allert.addAction(UIAlertAction.init(title: "Save", style: .default, handler: { (act) in
            if let text = allert.textFields?.first?.text{
                let porject = GlobalObjects.Project.init(rhombs: self.rhombs, circles: self.circles, squares: self.squares, triangles: self.triangles, texts: self.texts, cameras: self.cameras, lines: self.lines)
                let dataProject = try! PropertyListEncoder().encode(porject)
                self.saveToParce(projectData: dataProject, streetName: text)
                if self.keysArray.contains(self.keys.keyForAutosave){
                    for count in 0..<self.keysArray.count{
                        if self.keysArray[count] == self.keys.keyForAutosave{
                            self.keysArray.remove(at: count)
                        }
                    }
                }
                self.keysArray.append(text)
                UserDefaults.standard.set(self.keysArray, forKey: self.keys.keysForSave)
                UserDefaults.standard.set(dataProject, forKey: text)
                UserDefaults.standard.removeObject(forKey: self.keys.keyForAutosave)
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
        }))
        present(allert, animated: true, completion: nil)
    }
    
    func saveToParce(projectData: Data, streetName: String){
        let currentUser = PFUser.current()
        if currentUser != nil {
        let file = PFFileObject.init(data: projectData)
        let projectParce = PFObject.init(className: "Project")
        projectParce["streetName"] = streetName
        projectParce["data"] = file
        projectParce["autor"] = PFUser.current()
        projectParce.saveInBackground()
            print("Saved in cloud")
        }
        else {
            print("Please log in to save in cloud")
        }
    }
    
    func loginButtonText() {
        let currentUser = PFUser.current()
        if currentUser != nil {
            let userName = currentUser?.username as! String
            loginTF.setTitle("Log out: \(userName)", for: .normal)
        }
        else {
            loginTF.setTitle("Log In", for: .normal)
        }
        
    }
    
    func showLoadFromParceButton(){
        let currentUser = PFUser.current()
        if currentUser != nil {
            loadFromParceButton.isHidden = false
        }
    }
    
    @IBAction func save(_ sender: Any) {
        showTextField()
    }
    
    @IBAction func load(_ sender: Any) {
        
    }
    
    @IBAction func login(_ sender: Any) {
        let currentUser = PFUser.current()
        if currentUser == nil{let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.present(loginVC, animated: true, completion: nil)
            
        }
        else {
            let sv = UIViewController.displaySpinner(onView: self.view)
            PFUser.logOutInBackground { (error: Error?) in
                UIViewController.removeSpinner(spinner: sv)
                if (error == nil){
                    self.loginButtonText()
                }
                else {
                    if let descrip = error?.localizedDescription{
                        self.displayErrorMessage(message: descrip)
                    }
                    else {
                        self.displayErrorMessage(message: "error logging out")
                    }
                }
            }
        }
    }
    
    func displayErrorMessage(message:String) {
        let alertView = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertView.addAction(OKAction)
        if let presenter = alertView.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        self.present(alertView, animated: true, completion:nil)
    }
    
    @objc func loginSucceed(notification: Notification){
        loginButtonText()
        showLoadFromParceButton()
    }
    
    
}

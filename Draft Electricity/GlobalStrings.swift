//
//  GlobalStrings.swift
//  Draft Electricity
//
//  Created by Igor Litvin on 12/10/18.
//  Copyright © 2018 Igor Litvin. All rights reserved.
//

import UIKit

class GlobalStrings: NSObject {

    // keys for save to user defaults (for func save and load in ViewController)
    let lineNsDict = "two"
    let nsDict = "one"
    let textDict = "text"
    let nsTriangleTransormsDict = "nsTriangleTransormsDict"
    let nsRhombTransormsDict = "nsRhombTransormsDict"
    let nsSquareTransormsDict = "nsSquareTransormsDict"
    let nsTextTransormsDict = "nsTextTransormsDict"
    let nsCameraDict = "nsCameraDict"
    
    let rhombs = "rhombs"
    let circles = "circles"
    let squares = "squares"
    let triangles = "triangles"
    let texts = "texts"
    let cameras = "cameras"
    
    let keysForSave = "keys"
    let keyForAutosave = "autosave"
    
}

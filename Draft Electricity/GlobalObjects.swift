//
//  GlobalVariables.swift
//  Draft Electricity
//
//  Created by Igor Litvin on 12/10/18.
//  Copyright © 2018 Igor Litvin. All rights reserved.
//

import UIKit
import CoreLocation

protocol Shape: Codable {
    var latitude: CLLocationDegrees { get set }
    var longtitude: CLLocationDegrees { get set }
    var transform: CGAffineTransform { get set }
    var markerView: UIView { get }
}

class GlobalObjects: NSObject {
    
    struct Triangle: Shape {
        var latitude: CLLocationDegrees
        var longtitude: CLLocationDegrees
        var transform: CGAffineTransform
        var markerView: UIView {
            let view = TriangleView(frame: CGRect(x: 200, y: 200, width: 20 , height: 20))
            view.backgroundColor = .clear
            return view
        }
    }
    
    struct Square: Shape {
        var latitude: CLLocationDegrees
        var longtitude: CLLocationDegrees
        var transform: CGAffineTransform
        var markerView: UIView {
            let view = UIView(frame: CGRect(x: 200, y: 200, width: 10, height: 20))
            view.backgroundColor = .black
            return view
        }
    }
    
    struct Circle: Shape {
        var latitude: CLLocationDegrees
        var longtitude: CLLocationDegrees
        var transform: CGAffineTransform
        var markerView: UIView {
            let circleLayer = CircleView.init(frame: CGRect.init(x: 1, y: 1, width: 20, height: 20))
            circleLayer.circle(cX: 10, cY: 10, radius: 9, color: UIColor.orange.cgColor)
            return circleLayer
        }
    }
    
    struct Rhomb: Shape {
        var latitude: CLLocationDegrees
        var longtitude: CLLocationDegrees
        var transform: CGAffineTransform
        var markerView: UIView {
            let view = RhombView.init(frame: CGRect(x: 1, y: 1, width: 10 , height: 20))
            view.backgroundColor = .clear
            return view
        }
    }
    
    struct Text: Shape {
        var latitude: CLLocationDegrees
        var longtitude: CLLocationDegrees
        var transform: CGAffineTransform
        var text: String
        var markerView: UIView {
            let view = UIView.init(frame: CGRect(x: 10, y: 10, width: 100, height: 40))
            return view
        }
    }
    
    struct Camera:Encodable, Decodable {
        var latitude: CLLocationDegrees
        var longtitude: CLLocationDegrees
    }
    
    struct Project:Encodable, Decodable {
        var rhombs:Array<Data> = []
        var circles:Array<Data> = []
        var squares:Array<Data> = []
        var triangles:Array<Data> = []
        var texts:Array<Data> = []
        var cameras:Array<Data> = []
        var lines:Array<Data> = []
    }
    
    struct Line:Encodable, Decodable {
        var firstCoordinateLatitude: CLLocationDegrees
        var firstCoordinateLongtitude: CLLocationDegrees
        var seconCoordinateLatitude: CLLocationDegrees
        var seconCoordinateLongtitude: CLLocationDegrees
    }
    
}

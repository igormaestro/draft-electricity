//
//  LoadFromParceVC.swift
//  Draft Electricity
//
//  Created by Igor Litvin on 4/7/19.
//  Copyright © 2019 Igor Litvin. All rights reserved.
//

import UIKit
import Parse

class LoadFromParceVC: UIViewController {
    var objectsArray:[PFObject] = []
    var streetName:[String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
         let _ = UIViewController.displaySpinner(onView: self.view)
        NotificationCenter.default.addObserver(self, selector: #selector(dataRecived(notification:)), name: Notification.Name.dataRecived, object: nil)
        loadFromParse()
       
    }
    
    func loadFromParse(){
        let query = PFQuery.init(className: "Project")
        query.findObjectsInBackground { (objects, error) in
            guard let object = objects else {
                return
            }
            self.objectsArray = object
            print(" Object from load \(object.count)")
            print("in objectsArray \(self.objectsArray.count) items")
            NotificationCenter.default.post(name: Notification.Name.dataRecived, object: nil)
        }
    }

    func ejectStreetName(){
        objectsArray.forEach { (object) in
            let word = object["streetName"] as! String
            streetName.append(word)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("___________")
        if segue.identifier == "loadFromParceTVC"{
            if let viewController = segue.destination as? LoadFromParceTVC{
                ejectStreetName()
                viewController.tableArray = objectsArray
                viewController.streetName = streetName
              
            }
        }
    }
    
    @objc func dataRecived(notification: Notification){
        performSegue(withIdentifier:"loadFromParceTVC", sender: nil)

    }

}
